/*
Navicat MySQL Data Transfer

Source Server         : warehouse
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : warehouse

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-12-31 02:21:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Product 1', '1');
INSERT INTO `products` VALUES ('2', 'Product 2', '1');
INSERT INTO `products` VALUES ('3', 'Product 3', '1');
INSERT INTO `products` VALUES ('4', 'Product 4', '1');
INSERT INTO `products` VALUES ('5', 'Product 5', '1');
INSERT INTO `products` VALUES ('6', 'Product 6', '1');

-- ----------------------------
-- Table structure for qty_warehouse_products
-- ----------------------------
DROP TABLE IF EXISTS `qty_warehouse_products`;
CREATE TABLE `qty_warehouse_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_warehouse` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `min_product` int(11) NOT NULL,
  `max_product` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `sold` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_id_warehouse` (`id_warehouse`),
  KEY `FK_id_product` (`id_product`),
  CONSTRAINT `FK_id_product` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_id_warehouse` FOREIGN KEY (`id_warehouse`) REFERENCES `warehouses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qty_warehouse_products
-- ----------------------------
INSERT INTO `qty_warehouse_products` VALUES ('1', '1', '1', '5', '90', '85', '5', '1');
INSERT INTO `qty_warehouse_products` VALUES ('2', '1', '2', '10', '50', '30', '15', '1');
INSERT INTO `qty_warehouse_products` VALUES ('3', '1', '3', '5', '25', '160', '0', '1');
INSERT INTO `qty_warehouse_products` VALUES ('4', '1', '4', '15', '100', '14', '0', '1');
INSERT INTO `qty_warehouse_products` VALUES ('5', '2', '4', '3', '15', '10', '0', '1');

-- ----------------------------
-- Table structure for warehouses
-- ----------------------------
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warehouses
-- ----------------------------
INSERT INTO `warehouses` VALUES ('1', 'Warehouse 1', '1');
INSERT INTO `warehouses` VALUES ('2', 'Warehouse 2', '1');
INSERT INTO `warehouses` VALUES ('3', 'Warehouse 3', '1');
INSERT INTO `warehouses` VALUES ('4', 'Warehouse 4', '1');
