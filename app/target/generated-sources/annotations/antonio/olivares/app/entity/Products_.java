package antonio.olivares.app.entity;

import antonio.olivares.app.entity.QtyWarehouseProducts;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-30T23:55:10")
@StaticMetamodel(Products.class)
public class Products_ { 

    public static volatile SingularAttribute<Products, String> name;
    public static volatile SingularAttribute<Products, Integer> id;
    public static volatile ListAttribute<Products, QtyWarehouseProducts> qtyWarehouseProductsList;
    public static volatile SingularAttribute<Products, Boolean> status;

}