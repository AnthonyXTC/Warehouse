package antonio.olivares.app.entity;

import antonio.olivares.app.entity.Products;
import antonio.olivares.app.entity.Warehouses;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-30T23:55:10")
@StaticMetamodel(QtyWarehouseProducts.class)
public class QtyWarehouseProducts_ { 

    public static volatile SingularAttribute<QtyWarehouseProducts, Integer> sold;
    public static volatile SingularAttribute<QtyWarehouseProducts, Integer> minProduct;
    public static volatile SingularAttribute<QtyWarehouseProducts, Warehouses> idWarehouse;
    public static volatile SingularAttribute<QtyWarehouseProducts, Products> idProduct;
    public static volatile SingularAttribute<QtyWarehouseProducts, Integer> id;
    public static volatile SingularAttribute<QtyWarehouseProducts, Integer> stock;
    public static volatile SingularAttribute<QtyWarehouseProducts, Integer> maxProduct;
    public static volatile SingularAttribute<QtyWarehouseProducts, Boolean> status;

}