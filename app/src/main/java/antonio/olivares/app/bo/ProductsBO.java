/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bo;

import antonio.olivares.app.entity.Products;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public class ProductsBO {

    private Products products;

    public ProductsBO() {
    }

    public ProductsBO(Products products) {
        this.products = products;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

}
