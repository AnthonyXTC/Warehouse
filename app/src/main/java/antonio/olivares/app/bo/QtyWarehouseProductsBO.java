/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bo;

import antonio.olivares.app.entity.QtyWarehouseProducts;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public class QtyWarehouseProductsBO {

    private QtyWarehouseProducts warehouseProducts;

    public QtyWarehouseProductsBO(QtyWarehouseProducts warehouseProducts) {
        this.warehouseProducts = warehouseProducts;
    }

    public QtyWarehouseProductsBO() {
    }

    public QtyWarehouseProducts getWarehouseProducts() {
        return warehouseProducts;
    }

    public void setWarehouseProducts(QtyWarehouseProducts warehouseProducts) {
        this.warehouseProducts = warehouseProducts;
    }

    public String getStyleClass() {
        if (warehouseProducts.getStock() != null) {
            if (warehouseProducts.getStock() <= warehouseProducts.getMinProduct()) {
                return "red_background";
            } else if (warehouseProducts.getStock() >= warehouseProducts.getMaxProduct()) {
                return "green_background";
            } else if (warehouseProducts.getSold() > 0 && warehouseProducts.getStock() > warehouseProducts.getSold()) {
                if (warehouseProducts.getStock() / warehouseProducts.getSold() > warehouseProducts.getSold()) {
                    return "yellow_background";
                }
            }
        }
        return "";
    }

}
