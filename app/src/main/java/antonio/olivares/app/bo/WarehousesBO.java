/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bo;

import antonio.olivares.app.entity.Warehouses;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public class WarehousesBO {

    private Warehouses warehouses;

    public WarehousesBO(Warehouses warehouses) {
        this.warehouses = warehouses;
    }

    public WarehousesBO() {
    }

    public Warehouses getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Warehouses warehouses) {
        this.warehouses = warehouses;
    }

}
