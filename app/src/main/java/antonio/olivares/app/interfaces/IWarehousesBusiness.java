/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.interfaces;

import antonio.olivares.app.bo.WarehousesBO;
import antonio.olivares.app.entity.Warehouses;
import java.util.List;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public interface IWarehousesBusiness {

    public List<WarehousesBO> obtenerListaActivos();

    public WarehousesBO guardarInformacion(Warehouses entity);

    public WarehousesBO obtenerPorId(Integer id);

    public WarehousesBO eliminar(Warehouses entity);
}
