/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.interfaces;

import antonio.olivares.app.bo.QtyWarehouseProductsBO;
import antonio.olivares.app.entity.QtyWarehouseProducts;
import java.util.List;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public interface IQtyWarehouseProductsBusiness {

    public List<QtyWarehouseProductsBO> obtenerListaActivos();

    public List<QtyWarehouseProductsBO> obtenerListaByIdWarehouse(Integer idWarehouse);

    public QtyWarehouseProductsBO guardarInformacion(QtyWarehouseProducts entity);

    public QtyWarehouseProductsBO obtenerPorId(Integer id);

    public QtyWarehouseProductsBO eliminar(QtyWarehouseProducts entity);
}
