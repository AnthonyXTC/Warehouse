/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.interfaces;

import antonio.olivares.app.bo.ProductsBO;
import antonio.olivares.app.entity.Products;
import java.util.List;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
public interface IProductsBusiness {

    public List<ProductsBO> obtenerListaActivos();

    public ProductsBO guardarInformacion(Products entity);

    public ProductsBO obtenerPorId(Integer id);

    public ProductsBO eliminar(Products entity);
}
