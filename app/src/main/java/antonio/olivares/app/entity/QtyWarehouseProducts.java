/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Entity
@Table(name = "qty_warehouse_products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "QtyWarehouseProducts.findAll", query = "SELECT q FROM QtyWarehouseProducts q")
    , @NamedQuery(name = "QtyWarehouseProducts.findById", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.id = :id")
    , @NamedQuery(name = "QtyWarehouseProducts.findByMinProduct", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.minProduct = :minProduct")
    , @NamedQuery(name = "QtyWarehouseProducts.findByMaxProduct", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.maxProduct = :maxProduct")
    , @NamedQuery(name = "QtyWarehouseProducts.findByStock", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.stock = :stock")
    , @NamedQuery(name = "QtyWarehouseProducts.findBySold", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.sold = :sold")
    , @NamedQuery(name = "QtyWarehouseProducts.findByStatus", query = "SELECT q FROM QtyWarehouseProducts q WHERE q.status = :status")})
public class QtyWarehouseProducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "min_product")
    private int minProduct;
    @Basic(optional = false)
    @NotNull
    @Column(name = "max_product")
    private int maxProduct;
    @Column(name = "stock")
    private Integer stock;
    @Column(name = "sold")
    private Integer sold;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private boolean status;
    @JoinColumn(name = "id_product", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Products idProduct;
    @JoinColumn(name = "id_warehouse", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Warehouses idWarehouse;

    public QtyWarehouseProducts() {
    }

    public QtyWarehouseProducts(Integer id) {
        this.id = id;
    }

    public QtyWarehouseProducts(Integer id, int minProduct, int maxProduct, boolean status) {
        this.id = id;
        this.minProduct = minProduct;
        this.maxProduct = maxProduct;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getMinProduct() {
        return minProduct;
    }

    public void setMinProduct(int minProduct) {
        this.minProduct = minProduct;
    }

    public int getMaxProduct() {
        return maxProduct;
    }

    public void setMaxProduct(int maxProduct) {
        this.maxProduct = maxProduct;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Products getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Products idProduct) {
        this.idProduct = idProduct;
    }

    public Warehouses getIdWarehouse() {
        return idWarehouse;
    }

    public void setIdWarehouse(Warehouses idWarehouse) {
        this.idWarehouse = idWarehouse;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QtyWarehouseProducts)) {
            return false;
        }
        QtyWarehouseProducts other = (QtyWarehouseProducts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "antonio.olivares.app.entity.QtyWarehouseProducts[ id=" + id + " ]";
    }
    
}
