/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bean;

import antonio.olivares.app.bo.WarehousesBO;
import antonio.olivares.app.entity.Warehouses;
import antonio.olivares.app.interfaces.IWarehousesBusiness;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Named(value = "warehousesBean")
@ViewScoped
public class WarehousesBean implements Serializable {

    @Inject
    private IWarehousesBusiness warehousesBusiness;

    private WarehousesBO warehousesBO;
    private List<WarehousesBO> list;

    @PostConstruct
    public void init() {
        list = warehousesBusiness.obtenerListaActivos();
    }

    public void editar(WarehousesBO warehousesBO) {
        this.warehousesBO = warehousesBusiness.obtenerPorId(warehousesBO.getWarehouses().getId());
    }

    public void eliminar(WarehousesBO warehousesBO) {
        warehousesBusiness.eliminar(warehousesBO.getWarehouses());
        list = warehousesBusiness.obtenerListaActivos();
        warehousesBO = null;
    }

    public void nuevo() {
        warehousesBO = new WarehousesBO(new Warehouses());
    }

    public void cancelar() {
        warehousesBO = null;
    }

    public void guardar() {

        if (!warehousesBO.getWarehouses().getName().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Information",
                    "Warehouse saved succesfully"));
            warehousesBusiness.guardarInformacion(warehousesBO.getWarehouses());
            list = warehousesBusiness.obtenerListaActivos();
            warehousesBO = null;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Information",
                    "Verify fields"));
        }

    }

    public String manageProducts(WarehousesBO wbo) {
        return "manageProducts.xhtml?faces-redirect=true&warehouse=" + wbo.getWarehouses().getId();
    }

    public WarehousesBO getWarehousesBO() {
        return warehousesBO;
    }

    public void setWarehousesBO(WarehousesBO personBO) {
        this.warehousesBO = personBO;
    }

    public List<WarehousesBO> getList() {
        return list;
    }

    public void setList(List<WarehousesBO> list) {
        this.list = list;
    }

}
