/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bean;

import antonio.olivares.app.bo.ProductsBO;
import antonio.olivares.app.entity.Products;
import antonio.olivares.app.interfaces.IProductsBusiness;
import antonio.olivares.app.interfaces.IProductsBusiness;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Named(value = "productsBean")
@ViewScoped
public class ProductsBean implements Serializable {

    @Inject
    private IProductsBusiness productsBusiness;

    private ProductsBO productsBO;
    private List<ProductsBO> list;

    @PostConstruct
    public void init() {
        list = productsBusiness.obtenerListaActivos();
    }

    public void editar(ProductsBO warehousesBO) {
        this.productsBO = productsBusiness.obtenerPorId(warehousesBO.getProducts().getId());
    }

    public void eliminar(ProductsBO warehousesBO) {

        productsBusiness.eliminar(warehousesBO.getProducts());
        list = productsBusiness.obtenerListaActivos();
        warehousesBO = null;
    }

    public void nuevo() {
        productsBO = new ProductsBO(new Products());
    }

    public void cancelar() {
        productsBO = null;
    }

    public void guardar() {

        if (!productsBO.getProducts().getName().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Information",
                    "Product saved succesfully"));
            productsBusiness.guardarInformacion(productsBO.getProducts());
            list = productsBusiness.obtenerListaActivos();
            productsBO = null;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Information",
                    "Verify fields"));
        }

    }

    public ProductsBO getProductsBO() {
        return productsBO;
    }

    public void setProductsBO(ProductsBO personBO) {
        this.productsBO = personBO;
    }

    public List<ProductsBO> getList() {
        return list;
    }

    public void setList(List<ProductsBO> list) {
        this.list = list;
    }

}
