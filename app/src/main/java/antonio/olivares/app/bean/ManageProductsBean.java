/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bean;

import antonio.olivares.app.bo.ProductsBO;
import antonio.olivares.app.bo.QtyWarehouseProductsBO;
import antonio.olivares.app.bo.WarehousesBO;
import antonio.olivares.app.entity.QtyWarehouseProducts;
import antonio.olivares.app.entity.Warehouses;
import antonio.olivares.app.interfaces.IProductsBusiness;
import antonio.olivares.app.interfaces.IQtyWarehouseProductsBusiness;
import antonio.olivares.app.interfaces.IWarehousesBusiness;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Named(value = "manageProductsBean")
@ViewScoped
public class ManageProductsBean implements Serializable {

    @Inject
    private IQtyWarehouseProductsBusiness warehouseProductsBusiness;
    @Inject
    private IWarehousesBusiness warehousesBusiness;
    @Inject
    private IProductsBusiness productsBusiness;

    private QtyWarehouseProductsBO warehouseProductsBO;
    private List<QtyWarehouseProductsBO> list;
    private WarehousesBO warehousesBO;
    private List<ProductsBO> productsBOs;
    private String idWarehouse;
    private boolean invaliteExist = false;

    @PostConstruct
    public void init() {
        idWarehouse = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("warehouse");
        validarParametro();

    }

    private void validarParametro() throws NumberFormatException {
        if (idWarehouse != null) {
            list = warehouseProductsBusiness.obtenerListaByIdWarehouse(Integer.parseInt(idWarehouse));
            warehousesBO = warehousesBusiness.obtenerPorId(Integer.parseInt(idWarehouse));
            productsBOs = productsBusiness.obtenerListaActivos();
        } else {
            goToWarehouses();
        }
    }

    public void goToWarehouses() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("warehouses.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(ManageProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void eliminar(QtyWarehouseProductsBO qtyWarehouseProductsBO) {

        warehouseProductsBusiness.eliminar(qtyWarehouseProductsBO.getWarehouseProducts());
        list = warehouseProductsBusiness.obtenerListaActivos();
        qtyWarehouseProductsBO = null;
    }

    public void nuevo() {
        warehouseProductsBO = new QtyWarehouseProductsBO(new QtyWarehouseProducts());
        warehouseProductsBO.getWarehouseProducts().setIdWarehouse(new Warehouses(Integer.parseInt(idWarehouse)));
    }

    public void editar(QtyWarehouseProductsBO qtyWarehouseProductsB) {
        warehouseProductsBO = warehouseProductsBusiness.obtenerPorId(qtyWarehouseProductsB.getWarehouseProducts().getId());
        invaliteExist = true;
    }

    public void cancelar() {
        warehouseProductsBO = null;
        invaliteExist = false;
    }

    public void guardar() {

        if (warehouseProductsBO.getWarehouseProducts().getIdProduct() != null && validarExisteProducto()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Information",
                    "Product saved succesfully"));
            warehouseProductsBusiness.guardarInformacion(warehouseProductsBO.getWarehouseProducts());
            list = warehouseProductsBusiness.obtenerListaByIdWarehouse(warehouseProductsBO.getWarehouseProducts().getIdWarehouse().getId());
            warehouseProductsBO = null;
            invaliteExist = false;
        } else {
            if (!validarExisteProducto()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Information",
                        "This product has already been added to the warehouse"));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Information",
                        "Verify fields"));
            }
        }

    }

    private boolean validarExisteProducto() {
        return list.stream().filter(q -> q.getWarehouseProducts().getIdProduct().equals(warehouseProductsBO.getWarehouseProducts().getIdProduct())).collect(Collectors.toList()).isEmpty() == !invaliteExist;
    }

    public String manageProducts(QtyWarehouseProductsBO wbo) {
        return "manageProducts.xhtml?faces-redirect=true+warehouse=" + wbo.getWarehouseProducts().getId();
    }

    public QtyWarehouseProductsBO getQtyWarehouseProductsBO() {
        return warehouseProductsBO;
    }

    public void setQtyWarehouseProductsBO(QtyWarehouseProductsBO personBO) {
        this.warehouseProductsBO = personBO;
    }

    public List<QtyWarehouseProductsBO> getList() {
        return list;
    }

    public void setList(List<QtyWarehouseProductsBO> list) {
        this.list = list;
    }

    public List<ProductsBO> getProductsBOs() {
        return productsBOs;
    }

    public void setProductsBOs(List<ProductsBO> productsBOs) {
        this.productsBOs = productsBOs;
    }

    public WarehousesBO getWarehousesBO() {
        return warehousesBO;
    }

    public void setWarehousesBO(WarehousesBO warehousesBO) {
        this.warehousesBO = warehousesBO;
    }

}
