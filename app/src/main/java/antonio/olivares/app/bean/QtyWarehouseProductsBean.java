/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.bean;

import antonio.olivares.app.bo.QtyWarehouseProductsBO;
import antonio.olivares.app.entity.QtyWarehouseProducts;
import antonio.olivares.app.interfaces.IQtyWarehouseProductsBusiness;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Named(value = "qtyWarehouseProductsBean")
@ViewScoped
public class QtyWarehouseProductsBean implements Serializable {

    @Inject
    private IQtyWarehouseProductsBusiness warehouseProductsBusiness;

    private QtyWarehouseProductsBO warehouseProductsBO;
    private List<QtyWarehouseProductsBO> list;

    private Integer addToStock;
    private Integer addToSold;

    @PostConstruct
    public void init() {
        list = warehouseProductsBusiness.obtenerListaActivos();
    }

    public void viewAddToSold(QtyWarehouseProductsBO warehousesBO) {
        this.warehouseProductsBO = warehouseProductsBusiness.obtenerPorId(warehousesBO.getWarehouseProducts().getId());
        addToSold = 0;
    }

    public void viewAddToStock(QtyWarehouseProductsBO warehousesBO) {
        this.warehouseProductsBO = warehouseProductsBusiness.obtenerPorId(warehousesBO.getWarehouseProducts().getId());
        addToStock = 0;
    }

    public void eliminar(QtyWarehouseProductsBO warehousesBO) {

        warehouseProductsBusiness.eliminar(warehousesBO.getWarehouseProducts());
        list = warehouseProductsBusiness.obtenerListaActivos();
        warehousesBO = null;
    }

    public void nuevo() {
        warehouseProductsBO = new QtyWarehouseProductsBO(new QtyWarehouseProducts());
    }

    public void cancelar() {
        warehouseProductsBO = null;
        addToSold = null;
        addToStock = null;
    }

    public void guardar() {
        if (addToSold != null) {
            setNewSold();
        } else if (addToStock != null) {
            setNewStock();
        }

        warehouseProductsBusiness.guardarInformacion(warehouseProductsBO.getWarehouseProducts());
        list = warehouseProductsBusiness.obtenerListaActivos();
        cancelar();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Information",
                "Information saved succesfully"));

    }

    public void setNewStock() {
        warehouseProductsBO.getWarehouseProducts().setStock(warehouseProductsBO.getWarehouseProducts().getStock() + addToStock);
    }

    public void setNewSold() {
        warehouseProductsBO.getWarehouseProducts().setStock(warehouseProductsBO.getWarehouseProducts().getStock() - addToSold);
        warehouseProductsBO.getWarehouseProducts().setSold(warehouseProductsBO.getWarehouseProducts().getSold() + addToSold);
    }

    public QtyWarehouseProductsBO getQtyWarehouseProductsBO() {
        return warehouseProductsBO;
    }

    public void setQtyWarehouseProductsBO(QtyWarehouseProductsBO personBO) {
        this.warehouseProductsBO = personBO;
    }

    public List<QtyWarehouseProductsBO> getList() {
        return list;
    }

    public void setList(List<QtyWarehouseProductsBO> list) {
        this.list = list;
    }

    public Integer getAddToStock() {
        return addToStock;
    }

    public void setAddToStock(Integer addToStock) {
        this.addToStock = addToStock;
    }

    public Integer getAddToSold() {
        return addToSold;
    }

    public void setAddToSold(Integer addToSold) {
        this.addToSold = addToSold;
    }

}
