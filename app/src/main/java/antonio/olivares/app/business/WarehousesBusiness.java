/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.business;

import antonio.olivares.app.bo.WarehousesBO;
import antonio.olivares.app.dao.WarehousesDAO;
import antonio.olivares.app.entity.Warehouses;
import antonio.olivares.app.interfaces.IWarehousesBusiness;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class WarehousesBusiness implements IWarehousesBusiness {

    @Inject
    private WarehousesDAO warehousesDAO;

    @Override
    public List<WarehousesBO> obtenerListaActivos() {
        return warehousesDAO.obtenerListaActivos();
    }

    @Override
    public WarehousesBO guardarInformacion(Warehouses entity) {
        return warehousesDAO.guardarInformacion(entity);
    }

    @Override
    public WarehousesBO obtenerPorId(Integer id) {
        return warehousesDAO.obtenerPorId(id);
    }

    @Override
    public WarehousesBO eliminar(Warehouses entity) {
        return warehousesDAO.eliminar(entity);
    }
}
