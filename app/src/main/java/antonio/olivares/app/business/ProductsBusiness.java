/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.business;

import antonio.olivares.app.bo.ProductsBO;
import antonio.olivares.app.dao.ProductsDAO;
import antonio.olivares.app.entity.Products;
import antonio.olivares.app.interfaces.IProductsBusiness;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class ProductsBusiness implements IProductsBusiness {

    @Inject
    private ProductsDAO productsDAO;

    @Override
    public List<ProductsBO> obtenerListaActivos() {
        return productsDAO.obtenerListaActivos();
    }

    @Override
    public ProductsBO guardarInformacion(Products entity) {
        return productsDAO.guardarInformacion(entity);
    }

    @Override
    public ProductsBO obtenerPorId(Integer id) {
        return productsDAO.obtenerPorId(id);
    }

    @Override
    public ProductsBO eliminar(Products entity) {
        return productsDAO.eliminar(entity);
    }
}
