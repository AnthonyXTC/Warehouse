/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.business;

import antonio.olivares.app.bo.QtyWarehouseProductsBO;
import antonio.olivares.app.dao.QtyWarehouseProductsDAO;
import antonio.olivares.app.entity.QtyWarehouseProducts;
import antonio.olivares.app.interfaces.IQtyWarehouseProductsBusiness;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class QtyWarehouseProductsBusiness implements IQtyWarehouseProductsBusiness {

    @Inject
    private QtyWarehouseProductsDAO warehouseProductsDAO;

    @Override
    public List<QtyWarehouseProductsBO> obtenerListaActivos() {
        return warehouseProductsDAO.obtenerListaActivos();
    }

    @Override
    public QtyWarehouseProductsBO guardarInformacion(QtyWarehouseProducts entity) {
        return warehouseProductsDAO.guardarInformacion(entity);
    }

    @Override
    public QtyWarehouseProductsBO obtenerPorId(Integer id) {
        return warehouseProductsDAO.obtenerPorId(id);
    }

    @Override
    public List<QtyWarehouseProductsBO> obtenerListaByIdWarehouse(Integer idWarehouse) {
        return warehouseProductsDAO.obtenerListaByIdWarehouse(idWarehouse);

    }

    @Override
    public QtyWarehouseProductsBO eliminar(QtyWarehouseProducts entity) {

        return warehouseProductsDAO.eliminar(entity);
    }
}
