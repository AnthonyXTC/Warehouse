/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.dao;

import antonio.olivares.app.bo.ProductsBO;
import antonio.olivares.app.config.Persistence;
import antonio.olivares.app.entity.Products;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class ProductsDAO {

    @Inject
    private Persistence persistence;

    public List<ProductsBO> obtenerListaActivos() {
        return new ArrayList<>(persistence.getEntityManager()
                .createQuery("SELECT c FROM Products c WHERE c.status = TRUE",
                        Products.class)
                .getResultList()).stream()
                .map(c -> new ProductsBO(c))
                .collect(Collectors.toList());
    }

    public ProductsBO guardarInformacion(Products entity) {
        if (entity.getId() != null) {
            persistence.getEntityManager().merge(entity);
        } else {
            entity.setId(0);
            entity.setStatus(Boolean.TRUE);
            persistence.getEntityManager().persist(entity);
        }
        return new ProductsBO(entity);
    }

    public ProductsBO eliminar(Products entity) {
        entity.setStatus(Boolean.FALSE);
        persistence.getEntityManager().merge(entity);
        return new ProductsBO(entity);
    }

    public ProductsBO obtenerPorId(Integer id) {
        return new ProductsBO(persistence.getEntityManager()
                .find(Products.class, id));
    }

}
