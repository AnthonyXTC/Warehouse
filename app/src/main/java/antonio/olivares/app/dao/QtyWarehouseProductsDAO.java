/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.dao;

import antonio.olivares.app.bo.QtyWarehouseProductsBO;
import antonio.olivares.app.config.Persistence;
import antonio.olivares.app.entity.QtyWarehouseProducts;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class QtyWarehouseProductsDAO {

    @Inject
    private Persistence persistence;

    public List<QtyWarehouseProductsBO> obtenerListaActivos() {
        return new ArrayList<>(persistence.getEntityManager()
                .createQuery("SELECT c FROM QtyWarehouseProducts c WHERE c.status = TRUE",
                        QtyWarehouseProducts.class)
                .getResultList()).stream()
                .map(c -> new QtyWarehouseProductsBO(c))
                .collect(Collectors.toList());
    }

    public List<QtyWarehouseProductsBO> obtenerListaByIdWarehouse(Integer idWarehouse) {
        return new ArrayList<>(persistence.getEntityManager()
                .createQuery("SELECT c FROM QtyWarehouseProducts c WHERE c.idWarehouse.id = :idWarehouse",
                        QtyWarehouseProducts.class)
                .setParameter("idWarehouse", idWarehouse)
                .getResultList()).stream()
                .map(c -> new QtyWarehouseProductsBO(c))
                .collect(Collectors.toList());
    }

    public QtyWarehouseProductsBO guardarInformacion(QtyWarehouseProducts entity) {
        if (entity.getId() != null) {
            persistence.getEntityManager().merge(entity);
        } else {
            entity.setId(0);
            entity.setStatus(Boolean.TRUE);
            entity.setSold(0);
            entity.setStock(0);
            persistence.getEntityManager().persist(entity);
        }

        return new QtyWarehouseProductsBO(entity);
    }

    public QtyWarehouseProductsBO eliminar(QtyWarehouseProducts entity) {
        entity.setStatus(Boolean.FALSE);
        persistence.getEntityManager().merge(entity);
        return new QtyWarehouseProductsBO(entity);
    }

    public QtyWarehouseProductsBO
            obtenerPorId(Integer id) {
        return new QtyWarehouseProductsBO(persistence.getEntityManager()
                .find(QtyWarehouseProducts.class,
                        id));
    }

}
