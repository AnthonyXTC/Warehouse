/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antonio.olivares.app.dao;

import antonio.olivares.app.bo.WarehousesBO;
import antonio.olivares.app.config.Persistence;
import antonio.olivares.app.entity.Warehouses;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Antonio Olivares <Antonio.Olivares J2EE Web Developer>
 */
@Stateless
public class WarehousesDAO {

    @Inject
    private Persistence persistence;

    public List<WarehousesBO> obtenerListaActivos() {
        return new ArrayList<>(persistence.getEntityManager()
                .createQuery("SELECT c FROM Warehouses c where c.status = TRUE",
                        Warehouses.class)
                .getResultList()).stream()
                .map(c -> new WarehousesBO(c))
                .collect(Collectors.toList());
    }

    public WarehousesBO guardarInformacion(Warehouses entity) {
        if (entity.getId() != null) {
            persistence.getEntityManager().merge(entity);
        } else {
            entity.setId(0);
            entity.setStatus(Boolean.TRUE);
            persistence.getEntityManager().persist(entity);
        }
        return new WarehousesBO(entity);
    }

    public WarehousesBO eliminar(Warehouses entity) {
        entity.setStatus(Boolean.FALSE);
        persistence.getEntityManager().merge(entity);
        return new WarehousesBO(entity);
    }

    public WarehousesBO obtenerPorId(Integer id) {
        return new WarehousesBO(persistence.getEntityManager()
                .find(Warehouses.class, id));
    }

}
